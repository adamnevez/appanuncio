import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import { Home } from './pages/Home';
import { ViewAnuncio } from './pages/ViewAnuncio';
import { RegisterAnuncio } from './pages/RegisterAnuncio';
import { EditAnuncio } from './pages/EditAnuncio';

import { Menu } from './components/Menu';

function App() {
  return (
    <div className="App">
      <Router>
        <Menu/>
        <Switch>            
          <Route exact path="/" component={Home}/>
          <Route path="/view-anuncio/:id" component={ViewAnuncio}/>
          <Route path="/register-anuncio" component={RegisterAnuncio}/>
          <Route path="/edit-anuncio/:id" component={EditAnuncio}/>
        </Switch>
      </Router>       
    </div>
  );
}

export default App;
