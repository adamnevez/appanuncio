import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Alert } from 'reactstrap';
import axios from 'axios';

import { api } from '../../config';

export const ViewAnuncio = (props) => {
	
	// console.log(props.match.params.id);
	const [data, setData] = useState([]);
	const [id] = useState(props.match.params.id);

	const [status, setStatus] = useState({
		type: '',
		mensagem: ''   
	});

	useEffect(() => {	
		const getAnuncio = async () => {
			await axios.get(api+"/visualizar/"+id)
			.then((response) => {
				// console.log(response);
				setData(response.data.anuncio);
			}).catch(() => {
				setStatus ({
					type: 'error',
					mensagem: 'Erro: try later'
				});
			});
		}
		getAnuncio();
	},[id]);

	return(
		<div>
			<Container>
				<div className="d-flex">
					<div className="mr-auto p-2">
						<h1>Detalhes</h1>
					</div>
					<div className="p-2">
						<Link to="/" className="btn btn-outline-info btn-sm">Listar</Link>&nbsp;
						<Link to={"/edit-anuncio/"+data.id} className="btn btn-outline-warning btn-sm">Editar</Link>
					</div>
				</div>

				<hr className="m-1"/>

				{status.type === 'error' ? <Alert color="danger">{status.mensagem}</Alert> : ""}
				
				<dl className="row">
					<dt className="col-sm-3">ID</dt>
					<dd className="col-sm-9">{data.id}</dd>

					<dt className="col-sm-3">Título</dt>
					<dd className="col-sm-9">{data.titulo}</dd>

					<dt className="col-sm-3">Descrição</dt>
					<dd className="col-sm-9">{data.descricao}</dd>
				</dl>
			</Container>
		</div>
	)
};