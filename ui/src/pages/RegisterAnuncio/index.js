import React, { useState, } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import { Container, Alert, Form, FormGroup, Label, Input, Button, Spinner } from 'reactstrap';
import { api } from '../../config';


export const RegisterAnuncio = () => {

	const [anuncio, setAnuncio] = useState({
		titulo: '',
		descricao: ''
	});

	const [status, setStatus] = useState({
		formSave: false,
		type: '',
		mensagem: ''   
	});

	const valorInput = e => setAnuncio({...anuncio, [e.target.name]:e.target.value});

	const cadAnuncio = async e => {
		e.preventDefault();
		
		setStatus({ formSave:true });

		const headers = {
			'Content-Type': 'application/json'
		}

		await axios.post(api+"/cadastrar", anuncio, { headers })
		.then((response) => {
			// console.log(response.data.message);
			if (response.data.error) {
				setStatus ({
					formSave: false,
					type: 'error',
					mensagem: response.data.message
				});
			}else{
				setStatus ({
					formSave: false,
					type: 'success',
					mensagem: response.data.message
				});
			}
		})
		.catch(() => {
			setStatus ({
				formSave: false,
				type: 'error',
				mensagem: 'Erro: try later'
			});
		})
	}

	return(
		<div>
			<Container>
				<div className="d-flex">
					<div className="mr-auto p-2">
						<h1>Adcionar Anúncios</h1>
					</div>
					<div className="p-2">
						<Link to="/" className="btn btn-outline-info btn-sm">Listar</Link>
					</div>
				</div>

				<hr className="m-1"/>

				{status.type === 'error' ? <Alert color="danger">{status.mensagem}</Alert> : ""}
				{status.type === 'success' ? <Alert color="success">{status.mensagem}</Alert> : ""}
				
				<Form onSubmit={cadAnuncio}>
					<FormGroup>
						<Label>Título</Label>
						<Input type="text" name="titulo" placeholder="Título do Anúncio" onChange={valorInput} required/>
					</FormGroup>

					<FormGroup>
						<Label>Descrição</Label>
						<Input type="text" name="descricao" placeholder="Descrição do Anúncio" onChange={valorInput}/>
					</FormGroup>
					<br/>
					{/*<Button type="submit" outline color="success">Register</Button>*/}
					{status.formSave ? 
					<Button type="submit" outline color="success" disabled>Adicionando...
						<Spinner size="sm" color="success"/>
					</Button> : <Button type="submit" outline color="success">Adicionar</Button>}
				</Form>
			</Container>
		</div>
	)
};