import React,{ useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import { Container, Alert, Form, FormGroup, Label, Input, Button, Spinner } from 'reactstrap';

import { api } from '../../config';

export const EditAnuncio = (props) => {

	const [id] = useState(props.match.params.id);
	const [titulo, setTitulo] = useState("");
	const [descricao, setDescricao] = useState("");

	const [status, setStatus] = useState({
		formSave: false,
		type: '',
		mensagem: ''   
	});

	const editAnuncio = async e => {
		e.preventDefault();

		setStatus({ formSave:true });

		const headers = {
			'Content-Type': 'application/json'
		}

		await axios.put(api+"/editar", {id, titulo, descricao}, { headers }) 
		.then((response) => {
			if (response.data.error){
				setStatus ({
					formSave:false,
					type: 'error',	
					mensagem: response.data.message
				});
			}else {
				setStatus ({
					formSave:false,
					type: 'success',	
					mensagem: response.data.message
				});
			}
		}).catch(() => {
			setStatus ({
				formSave:false,
				type: 'error',
				mensagem: 'Erro: try later'
			});
		});
	}

	useEffect(() => {	
		const getAnuncio = async () => {
			await axios.get(api+"/visualizar/"+id)
				.then((response) => {
					setTitulo(response.data.anuncio.titulo);
					setDescricao(response.data.anuncio.descricao);
					// console.log(response.data.anuncio.titulo);
				}).catch(() => {
					setStatus ({
						type: 'error',
						mensagem: 'Erro: try later'
					});
				})
		}
		getAnuncio();
	},[id]);


	return(
		<div>
			<Container>
				<div className="d-flex">
					<div className="mr-auto p-2">
						<h1>Editar Anúncios</h1>
					</div>
					<div className="p-2">
						<Link to="/" className="btn btn-outline-info btn-sm">Listar</Link>&nbsp;
						<Link to={"/view-anuncio/"+id} className="btn btn-outline-primary btn-sm">Detalhes</Link>
					</div>
				</div>
			
				<hr className="m-1"/>

				{status.type === 'error' ? <Alert color="danger">{status.mensagem}</Alert> : ""}
				{status.type === 'success' ? <Alert color="success">{status.mensagem}</Alert> : ""}

				<Form onSubmit={ editAnuncio }>
					<FormGroup>
						<Label>Título</Label>
						<Input type="text" name="titulo" placeholder="Title Adverts" value={titulo} onChange={e => setTitulo(e.target.value)}/>
					</FormGroup>

					<FormGroup>
						<Label>Descrição</Label>
						<Input type="text" name="descricao" placeholder="Description Adverts" value={descricao} onChange={e => setDescricao(e.target.value)}/>
					</FormGroup>
					<br/>
					{/*<Button type="submit" outline color="success">Register</Button>*/}
					{status.formSave ? 
					<Button type="submit" outline color="warning" disabled> Salvando...</Button> : 
					<Button type="submit" outline color="info">Salvar</Button>}
				</Form>
			</Container>
		</div>
	)
};