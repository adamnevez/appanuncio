import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Container, Table, Alert } from 'reactstrap';

import { api } from '../../config';

export const Home = () => {

	const [data, setData] = useState([]);
	const [status, setStatus] = useState ({
		type: '',
		mensagem: ''   
	});

	const getAnuncios = async () => {
		await axios.get(api)
		.then((response) => {
			// console.log(respose.data.anuncios);
			setData(response.data.anuncios);	
		}).catch(() => {
			setStatus ({
				type: 'error',
				mensagem: 'Erro: try later'
			});
		});
	}

	useEffect(() => {
		getAnuncios();
	},[]);

	const deleteAdverts = async (idAnuncio) => {
		console.log (idAnuncio);

		const headers = {
			'Content-Type': 'application/json'
		}

		await axios.delete(api+"/apagar/"+idAnuncio, { headers })
		.then((response) => {
			console.log(response.data.error);
			if (response.data.error){
				setStatus ({
					type: 'error',
					mensagem: response.data.message
				});
			}else {
				setStatus ({
					type: 'success',
					mensagem: response.data.message
				});
			}
		}).catch(() => {
			setStatus ({
				type: 'error',
				mensagem: 'Erro: try later'
			});
		});
	}

	return(
		<div>
			<Container>
				<div className="row">
					<div className="col-10 p-2">
						<h1>Home</h1>
					</div>
					<div className="col-2 p-2">
						<Link to="/register-anuncio" className="btn btn-outline-success btn-sm">Adicionar</Link>
					</div>
				</div>

				{status.type === 'error' ? <Alert color="danger">{status.mensagem}</Alert> : ""}
				{status.type === 'success' ? <Alert color="success">{status.mensagem}</Alert> : ""}
				
				<Table striped hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Título</th>
							<th>Descrição</th>
							<th className="text-center">Ações</th>
						</tr>
					</thead>
					<tbody>
						{data.map(item => (
							<tr key={item.id}>
								<td>{item.id}</td>
								<td>{item.titulo}</td>
								<td>{item.descricao}</td>
								<td className="text-center">
									<Link to={"/view-anuncio/"+item.id} className="btn btn-outline-primary btn-sm ">Detalhes</Link>&nbsp; 
									<Link to={"/edit-anuncio/"+item.id} className="btn btn-outline-warning btn-sm ">Editar</Link>&nbsp; 
									<span className="btn btn-outline-danger btn-sm" onClick={() => deleteAdverts(item.id)}>Apagar</span>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
			</Container>
		</div>
	);
};