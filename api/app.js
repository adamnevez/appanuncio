const express = require('express');
const cors = require('cors')
const app = express();

const Anuncio = require('./models/Anuncio');

app.use(express.json());

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin","*");
	res.header("Access-Control-Allow-Methods","GET, PUT, POST,DELETE");
	res.header("Access-Control-Allow-Headers","X-PINGOTHER, Content-Type, Authorization");
	app.use(cors());
	next();
});

// const db = require("./models/db");

app.get('/', async (req, res) => {
	await Anuncio.findAll({order : [['id','DESC']]}).then(function(anuncios){
  		res.json({anuncios});
  	});
});

app.get('/visualizar/:id', async (req, res) => {
	await Anuncio.findByPk(req.params.id)
	.then(anuncio => {
		return res.json({
			error: false,
			anuncio
		});
	}).catch(function(erro){
		return res.status(400).json({
			error: true,
			message: "Erro: Ad not found"
		});
	});
});



app.post('/cadastrar', async(req, res) => {
	const resultCad = await Anuncio.create(
		req.body
	).then(function(){
		return res.json({
			error: false,
			message: "Anúncio adicionado com sucesso."
		})
	}).catch(function(erro){
		return res.status(400).json({
			error: true,
			message: "Erro ao adcionar anúncio"
		});
	});
});


app.put('/editar', async(req, res) => {

	await sleep(3000);

	function sleep(ms){
		return new Promise((resolve) => {
			setTimeout(resolve,ms);
		});
	}

	await Anuncio.update(req.body, {
		where: {id: req.body.id}
	}).then(function(){
		return res.json({
			error: false,
			message: "Anúncio editado com sucesso"
		});
	}).catch(function(erro){
		return res.status(400).json({
			error: true,
			message: "Erro ao editar anúncio"
		});
	});
});

app.delete('/apagar/:id', async (req, res) => {
	await Anuncio.destroy({
		where: {id: req.params.id}
	}).then(function(){
		return res.json({
			error: false,
			message: "Anúncio apagado"
		});
	}).catch(function(erro){
		return res.status(400).json({
			error: true,
			message: "Erro ao apagar"
		});
	});
});

app.listen(8080, function(){
	console.log("Serve start in http://localhost:8080/");
});